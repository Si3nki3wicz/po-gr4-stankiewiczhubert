package pl.edu.uwm.wmii.StankiewiczHubert.java08;

import pl.imiajd.Stankiewicz.*;

import java.time.LocalDate;

public class zad1and2java08 {
    public static void main(String[] args) {

        Osobajava08[] ludzie = new Osobajava08[3];
        ludzie[0] = new Pracownikjava08("Konop", new String[]{"Krzysztof", "Jan"}, LocalDate.of(1980, 11,11), false, 3000, LocalDate.of(2015,6,6));
        ludzie[1] = new Studentjava08("Por", new String[]{"Anna"}, LocalDate.of(1999,3,3), true, "informatyka");
        ludzie[2] = new Studentjava08("Norek", new String[]{"Alicja"}, LocalDate.of(1999,5,5), true, "informatyka");

        ((Studentjava08)ludzie[1]).setŚredniaOcen(3.5);
        ((Studentjava08)ludzie[2]).setŚredniaOcen(5.0);
        for (Osobajava08 p : ludzie) {
            System.out.println(p.getNazwisko());
            System.out.println(p.getImiona());
            System.out.println(p.getDataUroddzenia());
            System.out.println(p.getOpis());
            if(p.getPłeć() == true){
                System.out.println("kobieta");
            }
            else{
                System.out.println("mężczyzna");
            }
            System.out.println();
        }
    }
}