package pl.edu.uwm.wmii.StankiewiczHubert.java08;

import pl.imiajd.Stankiewicz.Flet;
import pl.imiajd.Stankiewicz.Fortepian;
import pl.imiajd.Stankiewicz.Instrument;
import pl.imiajd.Stankiewicz.Skrzypce;
import java.time.LocalDate;
import java.util.ArrayList;

public class zad3java08 {

    public static void main(String[] args) {

        ArrayList<Instrument> Orkiestra = new ArrayList<>();
        Orkiestra.add(new Skrzypce("Bon", LocalDate.of(1996,4,20)));
        Orkiestra.add(new Fortepian("Bon", LocalDate.of(2001,5,20)));
        Orkiestra.add(new Flet("Bac", LocalDate.of(1999,5,20)));
        Orkiestra.add(new Flet("Bac", LocalDate.of(2020,1,16)));
        Orkiestra.add(new Skrzypce("Bon", LocalDate.of(2020,5,19)));

        for(int i=0;i<Orkiestra.size();i++){
            System.out.println(Orkiestra.get(i).dzwięk());
        }
        for(int j=0;j<Orkiestra.size();j++){
            System.out.println(Orkiestra.get(j).toString());
        }
    }
}