package pl.edu.uwm.wmii.StankiewiczHubert.java07;

import pl.imiajd.Stankiewicz.Nauczyciel;
import pl.imiajd.Stankiewicz.Osoba;
import pl.imiajd.Stankiewicz.Student;

public class zad4and5java07 {
    public static void main(String[] args) {

        Osoba osoba1 = new Osoba("Stankiewicz", 1999);
        System.out.println(osoba1.toString());

        Student student1 = new Student("Stankiewicz", 1999, "Inżynieria systemów informatycznych");
        System.out.println(student1.toString());

        Nauczyciel nauczyciel1 = new Nauczyciel("xxx", 1990, 3500);
        System.out.println(nauczyciel1.toString());


    }
}