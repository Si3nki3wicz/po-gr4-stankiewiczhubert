package pl.edu.uwm.wmii.StankiewiczHubert.java07;

import pl.imiajd.Stankiewicz.Adres;

public class zad2and3java07 {
    public static void main(String[] args) {

        Adres adres1 = new Adres("motylkowa", "1", 8, "Warszawa", "05-123");
        adres1.pokaz();

        Adres adres2 = new Adres("motylkowa", "2",  6,"Warszawa", "05-123");
        adres2.pokaz();

        Adres adres3 = new Adres("motylkowa", "3", 8, "Warszawa", "01-235");
        adres3.pokaz();

        System.out.println();

        System.out.println(adres1.przed(adres2));
        System.out.println(adres2.przed(adres3));

    }
}