package pl.edu.uwm.wmii.StankiewiczHubert.java13;

import java.util.PriorityQueue;
import java.util.Scanner;

public class zad1java13 {
    static PriorityQueue<Zadanie> zadania = new PriorityQueue<>();


    public static void main(String[] args) {
        System.out.print(
                        "Wpisz \"dodaj numer_priorytetu opis_zadania\" -> doda nowe zadanie np \"dodaj 4 Zadanie11\"\n" +
                        "Wpisz \"następne\" -> usunie najbardziej pilne zadanie\n" +
                        "Wpisz \"zakończ\" -> kończy program\n"
                        );

        String command = "";
        Scanner xan = new Scanner(System.in);
        while (!command.equals("zakończ")) {
            System.out.println("Podaj komende: ");
            command = xan.nextLine();

            if (command.split(" ")[0].equals("dodaj")) {
                StringBuffer opis = new StringBuffer();
                String[] tab = command.split(" ");
                for(int i = 2; i < tab.length; i++){
                    opis.append(tab[i]);
                }
                Zadanie zad = new Zadanie(Integer.parseInt(command.split(" ")[1]), opis.toString());
                dodaj(zad);
            } else if (command.equals("następne")) {
                next();
            }
        }
    }


    public static void next(){
        zadania.remove();
    }
    public static void dodaj(Zadanie zadanie){
        zadania.add(zadanie);
    }


    static class Zadanie implements Comparable<Zadanie> {

        private int priorytet;
        private String opis;

        Zadanie(int priorytet, String opis) {
            this.opis = opis;
            this.priorytet = priorytet;
        }

        public String getOpis() {
            return opis;
        }

        public int getPriorytet() {
            return priorytet;
        }

        @Override
        public int compareTo(Zadanie o) {
            //return Integer.compare(this.priorytet, o.getPriorytet());
            if(this.priorytet > o.getPriorytet()){
                return 1;
            }else if(this.priorytet < o.getPriorytet()){
                return -1;
            }else{
                return 0;
            }
        }
    }
}

