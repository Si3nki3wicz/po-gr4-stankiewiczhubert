package pl.edu.uwm.wmii.StankiewiczHubert.java13;

import javax.swing.text.html.HTMLDocument;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class zad3java13 {
    private static Map<Student, String> oceny = new TreeMap<>();
    public static void main(String[] args) {
        dodajStudenta(new Student(1, "Bak","Jan"),"3");
        dodajStudenta(new Student(2, "Cakowski","Adam"),"5");
        dodajStudenta(new Student(4, "Daczia","Jan"),"3");
        dodajStudenta(new Student(3, "Daczia","Jan"),"4");

        usunStudenta(1);
        zmienocene(4, "5");
        wypisz();

    }
    public static void dodajStudenta(Student student, String ocena){
        oceny.put(student, ocena);
    }
    public static void usunStudenta(Integer id){
        Iterator<Student> i = oceny.keySet().iterator();
        Student m;
        while(i.hasNext()){
            if((m = i.next()).id.equals(id)){
                oceny.remove(m);
                break;
            }
        }
    }
    public static void zmienocene(Integer id, String ocena){
        Iterator<Student> i = oceny.keySet().iterator();
        Student m;
        while(i.hasNext()){
            if((m = i.next()).id.equals(id)){
                oceny.replace(m, ocena);
                break;
            }
        }
    }
    public static void wypisz(){
        for(Student student : oceny.keySet()){
            System.out.println(student.id + ": " + student.nazwisko + " " + student.imie + ": " + oceny.get(student));
        }
    }

    static class Student implements Comparable<Student> {
        Integer id;
        String nazwisko;
        String imie;

        public Student(Integer id, String nazwisko, String imie) {
            this.id = id;
            this.nazwisko = nazwisko;
            this.imie = imie;
        }


        @Override
        public int compareTo(Student o) {
            if(nazwisko.equals(o.nazwisko)){
                if(imie.equals(o.imie)){
                    return Integer.compare(id,o.id);
                }else{
                    return imie.compareTo(o.imie);
                }
            }
            return nazwisko.compareTo(o.nazwisko);
        }
    }
}
