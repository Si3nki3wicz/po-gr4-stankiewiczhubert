package pl.edu.uwm.wmii.StankiewiczHubert.java13;

import java.util.Map;
import java.util.TreeMap;

public class zad2java13 {
    private static Map<String, String> oceny = new TreeMap<>();
    public static void main(String[] args) {
    dodajStudenta("Hubert","3");
    dodajStudenta("Adrian","5");
    dodajStudenta("Marek","4");
    dodajStudenta("Jan","3");
    usunStudenta("Marek");
    zmienocene("Hubert", "5");
    wypisz();

    }
    public static void dodajStudenta(String nazwisko, String ocena){
        oceny.put(nazwisko, ocena);
    }
    public static void usunStudenta(String nazwisko){
        oceny.remove(nazwisko);
    }
    public static void zmienocene(String nazwisko, String newocena){
        oceny.replace(nazwisko,newocena);
    }
    public static void wypisz(){
        for(String a : oceny.keySet()){
            System.out.println(a + ": " + oceny.get(a));
        }
    }
}
