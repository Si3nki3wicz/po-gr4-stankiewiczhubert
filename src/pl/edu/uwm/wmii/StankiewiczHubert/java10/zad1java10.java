package pl.edu.uwm.wmii.StankiewiczHubert.java10;

import pl.imiajd.Stankiewicz.Osobajava10;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class zad1java10 {
    public static void main(String[] args) {

        ArrayList<Osobajava10> grupa = new ArrayList<>();
        grupa.add(new Osobajava10("Stankiewicz", LocalDate.of(1999,12,31)));
        grupa.add(new Osobajava10("Por", LocalDate.of(1999,1,1)));
        grupa.add(new Osobajava10("Malinowski", LocalDate.of(1999,1,1)));
        grupa.add(new Osobajava10("Bekon", LocalDate.of(1998,5,25)));
        grupa.add(new Osobajava10("Bekon", LocalDate.of(1999,6,24)));

        for(int i=0;i<grupa.size();i++){
            System.out.println(grupa.get(i).toString());
        }
        System.out.println();
        Collections.sort(grupa);

        for(int i=0;i<grupa.size();i++){
            System.out.println(grupa.get(i).toString());
        }
    }
}