package pl.edu.uwm.wmii.StankiewiczHubert.java10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class zad3java10 {
    public static void main(String[] args) throws FileNotFoundException {
        String sciezka = "D:\\po-gr4-stankiewiczhubert\\plik3.txt";

        File file = new File(sciezka);
        Scanner in = new Scanner(file);
        ArrayList<String> lista = new ArrayList<>();
        while(in.hasNext()){
            lista.add(in.nextLine());
        }
        Collections.sort(lista);
        System.out.println(lista);
    }
}