package pl.edu.uwm.wmii.StankiewiczHubert.java11;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;


public class zad4java11 {
    static class ArrayUtil {
        public static <T extends Comparable<T>> int binSearch(T[] tab, T szukana) {
            int lewo = 0, prawo = tab.length - 1, srodek = 0;

            while (lewo <= prawo) {
                srodek = (lewo + prawo) / 2;
                if (tab[srodek].compareTo(szukana) == 0)
                    return srodek;
                else if (tab[srodek].compareTo(szukana) < 0)
                    lewo = srodek + 1;
                else
                    prawo = srodek - 1;
            }
            return -1;
        }

    }

    public static void main(String[] args) {
        Integer[] tab = {1,5,25,30,60,120,240};
        System.out.println(ArrayUtil.binSearch(tab, 5));
        LocalDate[] tab1 = {
                LocalDate.of(1999, Month.DECEMBER, 12),
                LocalDate.of(2000, Month.DECEMBER, 12),
                LocalDate.of(2001, Month.DECEMBER, 12),
                LocalDate.of(2003, Month.DECEMBER, 12),
                LocalDate.of(2003, Month.DECEMBER, 22),
                LocalDate.of(2003, Month.DECEMBER, 30)
        };
        System.out.println(ArrayUtil.binSearch(tab1, LocalDate.of(2003, Month.DECEMBER, 12)));

    }
}

