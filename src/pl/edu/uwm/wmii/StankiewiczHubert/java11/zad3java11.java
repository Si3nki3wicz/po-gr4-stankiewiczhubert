package pl.edu.uwm.wmii.StankiewiczHubert.java11;

import java.util.Arrays;

public class zad3java11 {
    public static void main(String[] args) {


        Integer[] tablicaNsorted = {3, 5, 7, 2, 4, 5};
        Arrays.stream(tablicaNsorted).forEach(arg -> System.out.print(arg + " "));
        System.out.println(ArrayUtil.isSorted(tablicaNsorted));

        Integer[] tablicasorted = {3, 5, 7, 9};
        Arrays.stream(tablicasorted).forEach(arg -> System.out.print(arg + " "));
        System.out.println(ArrayUtil.isSorted(tablicasorted));

    }
}
    class ArrayUtil {
        public static <T extends Comparable<T>> boolean isSorted(T[] tab){
            for(int i = 0; i < tab.length-1; i++){
                 if(tab[i].compareTo(tab[i+1]) > 0){
                     return false;
                 }
            }
            return true;
        }
    }

