package pl.edu.uwm.wmii.StankiewiczHubert.java11;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;


public class zad5java11 {
    static class ArrayUtil {
        public static <T extends Comparable<T>> void selectionSort(T[] tab) {
            int n = tab.length;

            for (int i = 0; i < n - 1; i++) {
                int min_idx = i;
                for (int j = i + 1; j < n; j++)
                    if (tab[j].compareTo(tab[min_idx]) < 0)
                        min_idx = j;

                T temp = tab[min_idx];
                tab[min_idx] = tab[i];
                tab[i] = temp;
            }
        }

    }

    public static void main(String[] args) {
        Integer[] tab = {5,25,1,160,3,5,9,20};
        ArrayUtil.selectionSort(tab);
        for(int a : tab){
            System.out.print(a + " ");
        }
        LocalDate[] tab1 = {
                LocalDate.of(3999, Month.DECEMBER, 1),
                LocalDate.of(2000, Month.DECEMBER, 6),
                LocalDate.of(2005, Month.DECEMBER, 1),
                LocalDate.of(2003, Month.DECEMBER, 12),
                LocalDate.of(2009, Month.DECEMBER, 22),
                LocalDate.of(2003, Month.DECEMBER, 30)
        };
        System.out.println("\n");
        ArrayUtil.selectionSort(tab1);
        for(LocalDate a: tab1){
            System.out.println(a + " ");
        }
    }
}

