package pl.edu.uwm.wmii.StankiewiczHubert.java11;

import pl.edu.uwm.wmii.StankiewiczHubert.java11.ArrayAlg;
import pl.edu.uwm.wmii.StankiewiczHubert.java11.Pair;

public class zad2java11 {
    public static void main(String[] args) {

        String[] words = {"Ala", "ma", "psa", "i", "kota"};
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        mm = PairUtil.swap(mm);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());

    }
}

class PairUtil {
    public static <T> Pair<T> swap(Pair<T> a)
    {
        return new Pair<T>(a.getSecond(), a.getFirst());
    }
}

