package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2Dlab02 {

    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;

    }

    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min, max);
            System.out.println(tab[i]);
        }
    }



    public static void dodatnie (int tab[]){
        int plus = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] > 0){
                plus = plus + tab[i];
            }
        }
        System.out.println("Suma liczb dodatnich: " + plus);
    }
    public static void ujemne (int tab[]){
        int minus = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] < 0){
                minus = minus + tab[i];
            }
        }
        System.out.println("Suma liczb ujemnych: " + minus);
    }



    public static void main(String[] args){
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc od 1 do 100:");
        int n = xin.nextInt();

        int[] tab = new int[n];
        generate(tab, n, -999, 999);
        dodatnie(tab);
        ujemne(tab);
    }

}
