package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad3lab02 {

    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;

    }

    public static void generate(int[][] tab, int m, int n, int min, int max){
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                tab[i][j] = getRandomNum(min, max);
                System.out.print(tab[i][j] + " ");
            }
            System.out.print("\n");
        }
        System.out.println("");
    }

    public static void mnorzenie(int[][] tab, int[][] tab2, int[][] tabwynik){
        for(int i=0; i<tab.length; i++){
            for(int j=0; j<tab.length; j++){
                for(int l = 0; l < tab2.length; l++){
                    tabwynik[i][j] = tabwynik[i][j] + tab[i][l] * tab2[l][j];
                }
                System.out.print(tabwynik[i][j] + " ");
            }
            System.out.print("\n");
        }
        System.out.println("");
    }

    public static void main(String[] args){
        System.out.println("\n" + "Podaj liczbe m");
        Scanner em = new Scanner(System.in);
        int m = em.nextInt();
        System.out.println("Podaj liczbe n");
        Scanner en = new Scanner(System.in);
        int n = en.nextInt();
        System.out.println("Podaj liczbe k");
        Scanner ka = new Scanner(System.in);
        int k = ka.nextInt();

        int[][] tab = new int[m][n];
        int[][] tab2 = new int[n][k];
        int[][] tabwynik = new int[m][k];
        System.out.println("Pierwsza macierz");
        generate(tab, m, n, -5, 5);
        System.out.println("Druga macierz");
        generate(tab2, n, k, -5, 5);
        System.out.println("Wymnorzone macierze");
        mnorzenie(tab,tab2,tabwynik);

    }

}
