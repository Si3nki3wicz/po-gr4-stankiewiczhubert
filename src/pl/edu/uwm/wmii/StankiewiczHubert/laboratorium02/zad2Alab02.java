package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2Alab02 {

    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;

    }

    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min, max);
            System.out.println(tab[i]);
        }
    }

    public static void niepa (int tab[]){
        int niepa = 0;
        for(int elem: tab){
            if(elem % 2 != 0){
                niepa++;
            }
        }
        System.out.println("Ilosc liczb nieparzystych: " + niepa);
    }
    public static void parzyste (int tab[]){
        int parzyste = 0;
        for(int elem: tab){
            if(elem % 2 == 0){
                parzyste++;
            }
        }
        System.out.println("Ilosc liczb parzystych: " + parzyste);
    }

    public static void main(String[] args){
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc od 1 do 100:");
        int n = xin.nextInt();

        int[] tab = new int[n];
        generate(tab, n, -999, 999);
        parzyste(tab);
        niepa(tab);


    }




}
