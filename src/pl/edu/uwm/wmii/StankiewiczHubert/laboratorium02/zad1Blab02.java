package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1Blab02 {
    public static void main(String[] args) {
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int n = xin.nextInt();
        int[] array = new int[n];
        Random zhao = new Random();
        for(int i = 0; i < n; i++){
            array[i] = zhao.nextInt(1999)-999;
            System.out.print(array[i] + ", ");
        }
        int minus = 0;
        int plus = 0;
        int zero = 0;
        for(int i = 0; i < n; i++){
            if(array[i] < 0){
                minus++;
            }
            if(array[i] > 0){
                plus++;
            }
            if(array[i] == 0){
                zero++;
            }
        }
        System.out.println("\n" + "Ilosc liczb zerowych:" + zero + "\n" + "Ilosc liczb ujemnych:" + minus + "\n" + "Ilosc liczb dodatnich:" + plus);
    }
}