package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1Dlab02 {
    public static void main(String[] args) {
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int n = xin.nextInt();
        int[] array = new int[n];
        Random zhao = new Random();
        for(int i = 0; i < n; i++){
            array[i] = zhao.nextInt(1999)-999;
            System.out.print(array[i] + ", ");
        }
        int minus = 0;
        int plus = 0;

        for(int i = 0; i < n; i++){
            if(array[i] < 0){
                minus = minus + array[i];
            }
            if(array[i] > 0){
                plus = plus + array[i];
            }
        }
        System.out.println("\n" + "Suma liczb ujemnych:" + minus + "\n" + "Suma liczb dodatnich:" + plus);
    }
}