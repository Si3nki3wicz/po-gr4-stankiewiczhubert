package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2Blab02 {

    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;

    }

    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min, max);
            System.out.println(tab[i]);
        }
    }



    public static void doda (int tab[]){
        int plus = 0;
        for(int elem: tab){
            if(elem > 0){
                plus++;
            }
        }
        System.out.println("Ilosc liczb dodatnich: " + plus);
    }
    public static void ujem (int tab[]){
        int ujemne = 0;
        for(int elem: tab){
            if(elem < 0){
                ujemne++;
            }
        }
        System.out.println("Ilosc liczb ujemnych: " + ujemne);
    }
    public static void zero (int tab[]){
        int zerowe = 0;
        for(int elem: tab){
            if(elem == 0){
                zerowe++;
            }
        }
        System.out.println("Ilosc liczb zerowych: " + zerowe);
    }



    public static void main(String[] args){
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc od 1 do 100:");
        int n = xin.nextInt();

        int[] tab = new int[n];
        generate(tab, n, -999, 999);
        doda(tab);
        ujem(tab);
        zero(tab);


    }




}
