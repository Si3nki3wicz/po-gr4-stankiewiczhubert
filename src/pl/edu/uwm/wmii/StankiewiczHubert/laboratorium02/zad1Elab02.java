package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1Elab02 {
    public static void main(String[] args) {
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int n = xin.nextInt();
        int[] array = new int[n];
        Random zhao = new Random();
        for(int i = 0; i < n; i++){
            array[i] = zhao.nextInt(1999)-999;
            System.out.print(array[i] + ", ");
        }

        int max = 0;
        int maxw = 0;

        for(int i = 0; i < n; i++){
            if(array[i] > 0){
                maxw++;
            }
            if(array[i] < 0){
                if(maxw > max){
                    max = maxw;
                }
                maxw = 0;
            }
        }
        if(max > maxw){
            maxw = max;
        }
        System.out.println("\n" + "Najdluzszy ciag liczb dodatnich:" + maxw);
    }
}