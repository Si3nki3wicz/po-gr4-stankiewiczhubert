package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2Clab02 {

    public static int getRandomNum(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;

    }

    public static void generate(int[] tab, int n, int min, int max) {
        for (int i = 0; i < n; i++) {
            tab[i] = getRandomNum(min, max);
            System.out.println(tab[i]);
        }
    }


    public static void maximum(int tab[]) {
        int max = tab[0];
        int maxw = 1;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > max) {
                max = tab[i];
                maxw = 1;
            } else if (tab[i] == max) {
                maxw++;
            }
        }
        System.out.println("Liczba maksymalna: " + max + "\n" + "Ilosc liczb najwiekszych: " + maxw);
    }

        public static void main (String[] args){
            Scanner xin = new Scanner(System.in);
            System.out.println("Podaj wartosc od 1 do 100:");
            int n = xin.nextInt();

            int[] tab = new int[n];
            generate(tab, n, -999, 999);
            maximum(tab);


        }
    }



