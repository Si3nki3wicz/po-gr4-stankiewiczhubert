package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1Alab02 {
    public static void main(String[] args) {
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int n = xin.nextInt();
        int[] array = new int[n];
        Random zhao = new Random();
        for(int i = 0; i < n; i++){
            array[i] = zhao.nextInt(1999)-999;
            System.out.print(array[i] + ", ");
        }
        int parzyste = 0;
        int niepa = 0;
        for(int i = 0; i < n; i++){
            if(array[i] % 2 == 0){
                parzyste++;
            }
            if(array[i] % 2 != 0){
                niepa++;
            }
        }
        System.out.println("\n" + "Ilosc liczb parzystych:" + parzyste + "\n" + "Ilosc liczb nieparzystych:" + niepa);
    }
}