package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1Clab02 {
    public static void main(String[] args) {
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int n = xin.nextInt();
        int[] array = new int[n];
        Random zhao = new Random();
        for(int i = 0; i < n; i++){
            array[i] = zhao.nextInt(1999)-999;
            System.out.print(array[i] + ", ");
        }
        int max = array[0];
        int maxw = 1;
        for(int i = 1; i < n; i++){
            if(array[i] > max){
                max = array[i];
                maxw = 1;
            }else if(array[i] == max){
                maxw++;
            }
        }
        System.out.println("\n" + "Najwieksza liczba:" + max + "\n" + "Najwieksza liczba wystapila:" + maxw);
    }
}