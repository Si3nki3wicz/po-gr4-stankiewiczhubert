package pl.edu.uwm.wmii.StankiewiczHubert.kolokwium01;

import java.util.Random;
import java.util.Scanner;

public class zadanie1kolos {

    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;

    }

    public static void generate(int[] tab, int n, int min, int max){
        for(int i=0; i<n; i++){
            tab[i] = getRandomNum(min, max);
            System.out.println(tab[i]);
        }
    }



    public static void ileliczb (int tab[]){
        int x = 0;
        int y = 0;
        int z = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] < -5){
                x++;

            }
            if(tab[i] > 5){
                y++;
            }
            if(tab[i] == -5){
                z++;
            }
        }
        System.out.print("Ilosc liczb wiekszych od 5: " + y + "\n" + "Ilosc liczb mniejszych od -5: " + x + "\n" + "Ilosc liczb rownych -5: " + z);
    }


    public static void main(String[] args){
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj ilosc randomowych liczb: ");
        int n = xin.nextInt();

        int[] tab = new int[n];
        generate(tab, n, -15, 15);
        ileliczb(tab);
    }

}