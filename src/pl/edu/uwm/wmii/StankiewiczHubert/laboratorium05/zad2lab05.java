package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium05;

import java.util.ArrayList;

public class zad2lab05 {

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> ab = new ArrayList(a.size() + b.size());
        int z = 0;
        if(b.size() < a.size() || b.size() == a.size()) {
            for (int i = 0; i < b.size(); i++) {
                    ab.add(i + z, a.get(i));
                    ab.add(i + 1 + z, b.get(i));
                    z++;
            }
            for(int j = b.size(); j < a.size(); j++){
                ab.add(j + z, a.get(j));
            }

        }
        if(a.size() < b.size()) {
            for (int i = 0; i < a.size(); i++) {
                ab.add(i + z, a.get(i));
                ab.add(i + 1 + z, b.get(i));
                z++;
            }
            for(int j = a.size(); j < b.size(); j++){
                ab.add(j + z, b.get(j));
            }

        }
        return ab;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(6);
        for (int i = 0; i < 6; i++) {
            a.add(i, i + 4);
        }

        ArrayList<Integer> b = new ArrayList<Integer>(3);
        for (int i = 0; i < 3; i++) {
            b.add(i, b.size() + i + 1);
        }

        System.out.print("Lista pierwsza: " + a + "\n");
        System.out.print("Lista druga: " + b + "\n");
        System.out.print("Listy razem (na przemian): " + merge(a, b));
    }



}
