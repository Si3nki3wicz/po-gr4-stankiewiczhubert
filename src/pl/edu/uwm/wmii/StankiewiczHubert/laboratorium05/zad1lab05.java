package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium05;

import java.util.ArrayList;

public class zad1lab05 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(6);
        for (int i = 0; i < 6; i++) {
            a.add(i, i + 4);
        }

        ArrayList<Integer> b = new ArrayList<Integer>(3);
        for (int i = 0; i < 3; i++) {
            b.add(i, b.size() + i + 1);
        }

        System.out.print("Lista pierwsza: " + a + "\n");
        System.out.print("Lista druga: " + b + "\n");
        System.out.print("Listy razem: " + append(a, b));
    }


    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> ab = new ArrayList<Integer>();
        ab.addAll(0, a);
        ab.addAll(a.size(), b);
        return ab;
    }
}