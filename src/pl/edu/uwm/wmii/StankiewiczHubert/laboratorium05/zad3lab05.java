package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium05;

import java.util.ArrayList;

public class zad3lab05 {

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> ab = new ArrayList<Integer>();
        ab.addAll(0, a);
        ab.addAll(a.size(), b);

        int temp = 0;
        int zmiana = 1;
        while(zmiana > 0){
            zmiana = 0;
            for(int i = 0; i < ab.size() -1; i++){
                if(ab.get(i) > ab.get(i+1)){
                    temp = ab.get(i+1);
                    ab.set(i+1, ab.get(i));
                    ab.set(i, temp);
                    zmiana++;
                }
            }
        }

        return ab;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(6);
        for (int i = 0; i < 6; i++) {
            a.add(i, i + 4);
        }

        ArrayList<Integer> b = new ArrayList<Integer>(5);
        for (int i = 0; i < 5; i++) {
            b.add(i, b.size() + i + 1);
        }

        System.out.print("Lista pierwsza: " + a + "\n");
        System.out.print("Lista druga: " + b + "\n");
        System.out.print("Listy razem posortowane: " + mergeSorted(a,b) + "\n");
    }



}
