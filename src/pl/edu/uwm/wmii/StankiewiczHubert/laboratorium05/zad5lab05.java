package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium05;

import java.util.ArrayList;

public class zad5lab05 {

    public static void reverse(ArrayList<Integer> a) {
        for (int i = 0; i < (a.size())/2; i++) {
            int bank = a.get(i);
            a.set(i, a.get(a.size()-1-i));
            a.set(a.size()-1-i,bank);

        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(6);
        for (int i = 0; i < 6; i++) {
            a.add(i, i + 4);
        }

        System.out.print("Lista: " + a + "\n");
        reverse(a);
        System.out.print("Lista odwrocona: " + a + "\n");
    }
}
