package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium05;

import java.util.ArrayList;

public class zad04lab05 {
    public static ArrayList<Integer> reverse(ArrayList<Integer> a) {
        ArrayList<Integer> ab = new ArrayList(a.size());
        int x = 1;
            for (int i = 0; i < a.size(); i++) {
                ab.add(i, a.get(a.size()-x));
                x++;
            }

        return ab;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(6);
        for (int i = 0; i < 6; i++) {
            a.add(i, i + 4);
        }

        System.out.print("Lista: " + a + "\n");
        System.out.print("Lista odwrocona " + reverse(a));
    }
}
