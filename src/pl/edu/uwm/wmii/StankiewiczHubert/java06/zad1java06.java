package pl.edu.uwm.wmii.StankiewiczHubert.java06;


public class zad1java06
{
    public static void main(String[] args)
    {
        RachunekBankowy bankowicz1 = new RachunekBankowy(5113);
        RachunekBankowy bankowicz2 = new RachunekBankowy(500000);

        RachunekBankowy.setRocznaStopaProcentowa(0.09);
        System.out.print("miesiac 1: "+ bankowicz1.oblicz() + "\n");
        System.out.print("miesiac 1: "+ bankowicz2.oblicz() + "\n");

        RachunekBankowy.setRocznaStopaProcentowa(0.1);
        System.out.print("miesiac 2: "+ bankowicz1.oblicz() + "\n");
        System.out.print("miesiac 2: "+ bankowicz2.oblicz() + "\n");

    }
}

class RachunekBankowy
{
    public RachunekBankowy(double saldo)
    {

        this.saldo = saldo;

    }
    public double oblicz()
    {
        double ods=(saldo * rocznaStopaProcentowa)/12;
        saldo = saldo + ods;
        return saldo;
    }
    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa)
    {
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }

    private double saldo;
    static double rocznaStopaProcentowa;
}