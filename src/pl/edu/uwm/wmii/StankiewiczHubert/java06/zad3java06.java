package pl.edu.uwm.wmii.StankiewiczHubert.java06;


public class zad3java06 {

    public static void main(String[] args) {
        Pracownikjava06[] personel = new Pracownikjava06[4];


        personel[0] = new Pracownikjava06("Jerzy Berek", 20000, 1999, 5, 5);
        personel[1] = new Pracownikjava06("Bartek Bak", 10500, 1998, 1, 1);
        personel[2] = new Pracownikjava06("Kamil Krac", 2555, 1997, 7, 7);
        personel[3] = new Pracownikjava06("Ania Kolczyk", 100000, 1989, 12, 30);

        for (Pracownikjava06 x : personel) {
            x.zwiekszPobory(40);
        }


        for (Pracownikjava06 x : personel) {
            System.out.print("nazwisko = " + x.nazwisko() + "\tid = " + x.id());
            System.out.print("\tpobory = " + x.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", x.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownikjava06.getNextId();
        System.out.println("Następny dostępny id = " + n);

    }
}