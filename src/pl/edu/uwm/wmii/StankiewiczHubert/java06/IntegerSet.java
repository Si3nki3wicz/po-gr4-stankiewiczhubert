package pl.edu.uwm.wmii.StankiewiczHubert.java06;

import java.util.ArrayList;
import java.util.Arrays;

public class IntegerSet {

    boolean[] tab = new boolean[100];

    public void wypisz(){
        for(int i =1;i< tab.length;i++){
            System.out.print(tab[i] + " | ");
        }
    }


    static int x = 0;
    public static ArrayList<Integer> union(boolean tab1[], boolean tab2[]){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i=0;i<tab1.length;i++){
            if(tab1[i] == true){
                wynik.add(i);
            }
        }
        for(int j=0;j< tab2.length;j++){
            for(int q=0;q<wynik.size();q++){
                if(tab2[j] == true && j == wynik.get(q)){
                    x = x +1;
                }
            }
            if(x == 0 && tab2[j] == true){
                wynik.add(j);
            }
            x = 0;
        }
        return wynik;
    }


    public  static ArrayList<Integer> intersection(boolean tab1[], boolean tab2[]){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i=0;i<tab1.length;i++){
            for(int j=0;j<tab2.length;j++){
                if(i == j && tab1[i] == tab2[j] && tab1[i] == true &&tab2[j] == true){
                    wynik.add(i);
                }
            }
        }
        return wynik;
    }


    public void insertElement(int n){
        if(n>0 && n<100) {
            tab[n] = true;
        }
    }


    public void deleteElemnt(int n){
        if(n>0 && n<100) {
            tab[n] = false;
        }
    }


    public String toString(){
        StringBuffer wynik = new StringBuffer();
        for(int i=0;i< tab.length;i++){
            if(tab[i] == true){
                wynik.append(i);
                wynik.append(" ");
            }
        }
        return wynik.toString();

    }


    public boolean equals(Object x) {
        if (this == x){
            return true;
        }
        if (x == null || getClass() != x.getClass()){
            return false;
        }
        IntegerSet that = (IntegerSet) x;
        return Arrays.equals(tab, that.tab);

    }

}