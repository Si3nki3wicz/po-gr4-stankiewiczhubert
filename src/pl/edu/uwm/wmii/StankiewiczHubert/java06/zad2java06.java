package pl.edu.uwm.wmii.StankiewiczHubert.java06;

import java.util.ArrayList;

public class zad2java06 {
    public static void main(String[] args) {

        IntegerSet zbior1 = new IntegerSet();
        zbior1.tab[10] = true;
        zbior1.tab[5] = true;
        zbior1.tab[15] = true;
        zbior1.tab[1] = true;
        IntegerSet zbior2 = new IntegerSet();
        zbior2.tab[5] = true;
        zbior2.tab[3] = true;
        zbior2.tab[1] = true;
        zbior2.tab[7] = true;
        zbior2.tab[20] = true;


        System.out.println("sumaMnogosciowa");
        ArrayList<Integer> sumaMnogosciowa = IntegerSet.union(zbior1.tab, zbior2.tab);
        System.out.println(sumaMnogosciowa);

        System.out.println("iloczynMnogosciowy");
        ArrayList<Integer> iloczynMnogosciowy = IntegerSet.intersection(zbior1.tab, zbior2.tab);
        System.out.println(iloczynMnogosciowy);


        System.out.println("dodaje");
        zbior1.insertElement(3);
        zbior1.wypisz();

        System.out.println("usuwa");
        System.out.println();
        zbior1.deleteElemnt(3);
        zbior1.wypisz();

        System.out.println("zamienia na String");
        System.out.println();
        String str = zbior1.toString();
        System.out.println(str);


        IntegerSet zbior3 = new IntegerSet();
        zbior3.insertElement(10);
        zbior3.insertElement(5);
        zbior3.insertElement(15);
        zbior3.insertElement(1);


        System.out.println("porownoje listy 1 i 2 czy maja takie same wartosci");
        System.out.println(zbior1.equals(zbior2));
        System.out.println("porownoje listy 1 i 3 czy maja takie same wartosci");
        System.out.println(zbior1.equals(zbior3));





    }
}
