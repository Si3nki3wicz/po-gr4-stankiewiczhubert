package pl.edu.uwm.wmii.StankiewiczHubert.kolokwium2;

import java.util.ArrayList;
import pl.imiajd.Stankiewicz.Autor;
import pl.imiajd.Stankiewicz.Ksiazka;

public class zadanie1kolos {
    public static void main(String[] args)
    {
        ArrayList<Autor> autorzy = new ArrayList<>(4);
        autorzy.add(0,new Autor("Sienkiewicz", "sienkiewicz@op.pl", 'M'));
        autorzy.add(1,new Autor("Sienkiewicz", "sienkiwicza@op.pl", 'K'));
        autorzy.add(2,new Autor("Mickiewicz", "mickiewicz@op.pl", 'M'));
        autorzy.add(3,new Autor("Slowacki", "mickiewicz@op.pl", 'M'));
        System.out.println(autorzy);
        autorzy.sort(Autor::compareTo);
        System.out.println(autorzy);


        ArrayList<Ksiazka> ksiazki = new ArrayList<>(4);
        ksiazki.add(0,new Ksiazka("ksiazka1", "Sienkiewicz", 39.99));
        ksiazki.add(1,new Ksiazka("ksiazka2", "Sienkiewicz", 39.99));
        ksiazki.add(2,new Ksiazka("ksiazka3", "Mickiewicz", 50.00 ));
        ksiazki.add(3,new Ksiazka("ksiazka4", "Slowacki", 45.99 ));
        System.out.println(ksiazki);
        ksiazki.sort(Ksiazka::compareTo);
        System.out.println(ksiazki);
    }

}