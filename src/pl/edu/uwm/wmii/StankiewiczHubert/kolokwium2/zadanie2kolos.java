package pl.edu.uwm.wmii.StankiewiczHubert.kolokwium2;

import pl.imiajd.Stankiewicz.Autor;

import java.util.LinkedList;

public class zadanie2kolos {


    public static void main(String[] args) {
        LinkedList<String> ksiazki = new LinkedList<>();
        ksiazki.add(0, new Autor("Sienkiewicz", "sienkiewicz@op.pl", 'M').toString());
        ksiazki.add(1, new Autor("Sienkiewicz", "sienkiewicza@op.pl", 'K').toString());
        ksiazki.add(1, new Autor("Mickiewicz", "smi@op.pl", 'M').toString());
        ksiazki.add(1, new Autor("Mickiewicz", "mic@op.pl", 'K').toString());
        ksiazki.add(1, new Autor("Slowacki", "slowa@op.pl", 'M').toString());
        ksiazki.add(1, new Autor("Slowacki", "pisarz@op.pl", 'M').toString());
        ksiazki.add(1, new Autor("Slowacki", "ktos@op.pl", 'M').toString());
        ksiazki.add(1, new Autor("Sienkiewicz", "jakis.pl", 'K').toString());


        System.out.println("Lista przed 'redukuj'");
        wypiszliste(ksiazki);
        redukuj(ksiazki,2);
        System.out.println("\n" + "Lista po 'redukuj'");
        wypiszliste(ksiazki);

    }
    public static void wypiszliste (LinkedList<String> lista){
        for(int i = 0; i < lista.size(); i++){
            System.out.print(lista.get(i) + ", ");
        }
    }
    public static void redukuj (LinkedList<String> books,int n){
        for(int i = 1; i < ((books.size()+1)/n); i++){
            books.remove(n*i);
        }
    }
}