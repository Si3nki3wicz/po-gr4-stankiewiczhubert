package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie1i {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -7;
        sequence[1] = -5;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 5;
        sequence[7] = 7;
        sequence[8] = 9;
        sequence[9] = 11;
        double result = 0;


        int silnia = 1;
        for (int i = 0; i < 10; i++) {
            silnia *= i+1;
            System.out.println(silnia);
            result = (result + (sequence[i] * Math.pow(-1,i+2)) / silnia) ;
            System.out.println(result);
        }

    }

}