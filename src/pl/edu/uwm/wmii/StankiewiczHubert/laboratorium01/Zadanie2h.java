package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2h {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = 9;
        sequence[3] = 17;
        sequence[4] = 24;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = -7;
        sequence[8] = 4;
        sequence[9] = 10;

        int x = 0;
        for (int i = 0; i < 10; i++) {
                if(Math.abs(sequence[i]) < Math.pow(i+1,2)){
                    System.out.println(sequence[i]);
                    x++;
                }
        }
        System.out.println("ilosc: " + x);
    }
}