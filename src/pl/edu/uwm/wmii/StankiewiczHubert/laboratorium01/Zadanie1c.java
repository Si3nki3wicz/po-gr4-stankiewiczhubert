package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

public class Zadanie1c {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -7;
        sequence[1] = -5;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 5;
        sequence[7] = 7;
        sequence[8] = 9;
        sequence[9] = 11;
        int result = 0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i] < 0){
                result = result + sequence[i] * (-1);
            }else{
                result = result + sequence[i];
            }
        }
        System.out.println(result);
    }
}