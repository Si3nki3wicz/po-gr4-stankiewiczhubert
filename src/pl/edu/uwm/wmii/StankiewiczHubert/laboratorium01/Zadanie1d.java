package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie1d {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -7;
        sequence[1] = -5;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 5;
        sequence[7] = 7;
        sequence[8] = 9;
        sequence[9] = 11;
        double result = 0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i] > 0) {
                result = result + Math.sqrt(sequence[i]);
            }
            else{
                result = result + Math.sqrt(sequence[i] * (-1));
            }
        }
        System.out.println(result);
    }
}