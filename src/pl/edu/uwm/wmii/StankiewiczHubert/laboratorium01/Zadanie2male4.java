package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2male4 {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = 0;
        sequence[3] = 3;
        sequence[4] = 5;
        sequence[5] = -30;
        sequence[6] = 20;
        sequence[7] = -7;
        sequence[8] = -4;
        sequence[9] = -10;

        int min = sequence[0];
        int max = sequence[0];
        for (int i = 0; i < 10; i++) {
            if (min > sequence[i]) {
                min = sequence[i];
            } else if (max < sequence[i]) {
                max = sequence[i];
            }
        }
        System.out.println("Wartosc minimalna: " + min);
        System.out.println("Wartosc maksymalna: " + max);
    }
}