package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2male2 {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = 2;
        sequence[3] = 3;
        sequence[4] = 5;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = -7;
        sequence[8] = -4;
        sequence[9] = -10;

        int x = 0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i] > 0){
                x = x + (sequence[i] * 2);
            }
        }
        System.out.println(x);
    }
}