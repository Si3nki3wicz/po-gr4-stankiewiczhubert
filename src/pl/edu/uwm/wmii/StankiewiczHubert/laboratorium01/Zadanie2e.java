package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2e {

    public static void main(String[] args){
        double sequence[];
        sequence = new double[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = 7;
        sequence[3] = 17;
        sequence[4] = 34;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = 7;
        sequence[8] = 4;
        sequence[9] = 16;

        int x = 0;
        int silnia = 1;
        for (int i = 0; i < 10; i++) {
            silnia *= i+1;
            if( sequence[i] > Math.pow(2,i+1) && sequence[i] < silnia) {
                System.out.println(sequence[i]);
                x++;
            }

        }
        System.out.println("ilosc: " + x);
    }
}