package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2c {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -18;
        sequence[1] = -16;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 4;
        sequence[6] = 64;
        sequence[7] = 7;
        sequence[8] = 9;
        sequence[9] = 16;

        int x = 0;
        for (int i = 0; i < 10; i++) {
                if (Math.sqrt(sequence[i]) % 2 == 0) {
                    System.out.println(sequence[i]);
                    x++;
                }

        }
        System.out.println("ilosc: " + x);
    }
}