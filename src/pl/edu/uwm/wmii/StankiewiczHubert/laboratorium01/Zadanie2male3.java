package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium01;

import java.lang.Math;
public class Zadanie2male3 {

    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = 0;
        sequence[3] = 3;
        sequence[4] = 5;
        sequence[5] = 3;
        sequence[6] = 0;
        sequence[7] = -7;
        sequence[8] = -4;
        sequence[9] = -10;

        int x = 0;
        int y = 0;
        int z = 0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i] > 0){
                x++;
            }else if(sequence[i] < 0){
                y++;
            }else if(sequence[i] == 0){
                z++;
            }
        }
        System.out.println("ilosc liczb dodatnich: " + x);
        System.out.println("ilosc liczb ujemna: " + y);
        System.out.println("ilosc liczb zero: " + z);
    }
}