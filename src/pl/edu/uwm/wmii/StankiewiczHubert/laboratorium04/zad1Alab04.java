package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

public class zad1Alab04 {

   public static int coutChar (String tekst, char c){
        int licznik = 0;
        for(int i = 0; i < tekst.length(); i++){
            if(tekst.charAt(i) == c){
                licznik++;
            }
        }
        return licznik;
    }

    public static void main(String[] args){
        String jop = "cecylia cecylia";
        char a = 'c';
        System.out.print("Napis to: " + jop);
        System.out.print("Literka c powtorzyla sie " + coutChar(jop,a) + " razy");
    }

}
