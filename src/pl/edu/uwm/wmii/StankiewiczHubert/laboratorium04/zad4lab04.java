package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.math.BigInteger;

public class zad4lab04 {
    public static BigInteger ziarenko(int n){
        BigInteger x = new BigInteger("2");
        BigInteger licz = new BigInteger("0");
        BigInteger z = new BigInteger("0");
        BigInteger y = new BigInteger("0");
        for(int i = 0; i < (n*n); i++) {
          y = x.pow(i);
          z = z.add(licz.add(y));
        }

        return z;
    }

    public static void main(String[] args){
        System.out.println(ziarenko(25));
    }
}

