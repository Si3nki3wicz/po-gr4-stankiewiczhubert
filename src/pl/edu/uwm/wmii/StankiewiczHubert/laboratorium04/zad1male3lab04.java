package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad1male3lab04 {
    public static int coutChar(File nazwa, char c) throws FileNotFoundException {
        int licznik = 0;
        Scanner scan = new Scanner(nazwa);
        String str = scan.nextLine();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                licznik++;
            }
        }
        return licznik;
    }

    public static boolean porownaj(String nazwa1, String nazwa2, int n) {
        int i;
        for (i = 0; i < nazwa2.length(); i++) {
            if (nazwa1.charAt(n + i) != nazwa2.charAt(i)) {
                return false;
            }
        }
        return true;
    }


    public static int ilepowtozen(File nazwa1, String napis2) throws FileNotFoundException {
        int k = 0;
        int i;
        Scanner scan = new Scanner(nazwa1);
        String str = scan.nextLine();
        for (i = 0; i < str.length(); i++) {
            if (porownaj(str, napis2, i)) {
                k++;
            }
        }
        return k;
    }


    public static void main(String[] args) throws FileNotFoundException {
        String b = "tat";
        System.out.print("W slowie: " + " wyraz: " + b + " powtarza sie " + ilepowtozen(new File("plik2.txt"), b) + " razy.");

    }
}