package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.math.BigDecimal;

public class zad5lab05 {
    public static BigDecimal kapital( double k, double p, int n){
        BigDecimal x = new BigDecimal(k);
        BigDecimal y = new BigDecimal(p);
        BigDecimal proc = new BigDecimal("0");
        for(int i = 0; i < n; i++){
            proc = x.multiply(y);
            x = x.add(proc);
        }
        return x.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    }

    public static void main(String[] args){
        System.out.println(kapital(3000,0.13,3));
    }
}
