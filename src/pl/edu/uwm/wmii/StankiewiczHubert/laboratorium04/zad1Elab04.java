package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.util.Arrays;

public class zad1Elab04 {
    public static boolean porownaj(String napis1, String napis2, int n) {
        int i;
        for (i = 0; i < napis2.length(); i++) {
            if (napis1.charAt(n + i) != napis2.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static int[] where(String napis1, String napis2) {
        int k = 0;
        int i;

        for(i=0;  i < napis1.length(); i++) {
            if(porownaj(napis1,napis2,i)) {
                k++;

                }
            }
        int[] tablica = new int[k];
        int z = 0;
        for(i=0;  i < napis1.length(); i++) {
            if(porownaj(napis1,napis2,i)) {
                tablica[z] = i;
                z++;
            }
        }

        return tablica;
    }



    public static void main(String[] args){
        String a = "tata ma taki tatuaz";
        String b = "ta";
        System.out.print("Napis "+ "\"" + b + "\"" + " w zdaniu: " + a + "\n" + " wystepuje w indeksach: " + Arrays.toString(where(a,b)));

    }
}