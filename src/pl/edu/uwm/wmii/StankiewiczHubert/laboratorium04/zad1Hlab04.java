package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.util.Scanner;

public class zad1Hlab04 {
    public static String nice(String napis1) {

        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int u = xin.nextInt();

        Scanner xon = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int o = xon.nextInt();



        int p = 0;
        if(napis1.length() % u == 0) {
            p = 0;
            for (int l = 1; l < napis1.length(); l++) {
                if (l % u == 0) {
                    p++;
                }
            }
        }
        if(napis1.length() % u != 0) {
            p = 0;
            for (int l = 1; l < napis1.length(); l++) {
                if (l % u == 0) {
                    p++;
                }
            }
        }

        char[] charchar = new char[napis1.length() + p];
        StringBuffer st = new StringBuffer(napis1.length() + p);
        int kak = napis1.length()-1;
        int pap = 1;
        for(int j = 0; j < charchar.length; j++){
            if(pap % 4 == 0){
                charchar[j]=o;
            }else{
                charchar[j] = napis1.charAt(kak);
                kak--;
            }
            pap++;
        }
        for(int i = charchar.length - 1; i >= 0; i--){
            st.append(charchar[i]);
        }
        return st.toString();
    }

    public static void main(String[] args){
        Scanner xan = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        String t = xan.nextInt();
        System.out.println(nice(t));
    }
}