package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

public class zad1Blab04 {

   public static boolean porownaj(String napis1, String napis2, int n) {
       int i;
       for (i = 0; i < napis2.length(); i++) {
           if (napis1.charAt(n + i) != napis2.charAt(i)) {
               return false;
           }
       }
       return true;
   }


    public static int ilepowtozen(String napis1, String napis2) {
    int k = 0;
    int i;
    for(i=0;  i < napis1.length(); i++) {
        if(porownaj(napis1,napis2,i)) {
            k++;
        }
    }
    return k;
    }

    public static void main(String[] args){
        String a = "tata ma taki tatuaz";
        String b = "tat";
        System.out.print("W slowie: " + a + " wyraz: " + b + " powtarza sie " + ilepowtozen(a,b) + " razy.");

    }
}
