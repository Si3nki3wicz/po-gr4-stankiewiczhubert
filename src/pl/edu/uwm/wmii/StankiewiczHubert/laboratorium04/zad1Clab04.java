package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

public class zad1Clab04 {

    public static String middle(String napis1) {
        if(napis1.length() % 2 == 0){
           return napis1.substring((napis1.length() / 2 - 1) , (napis1.length() / 2 + 1));
        }else{
           return napis1.substring(napis1.length() / 2 ,napis1.length() / 2 + 1);
        }
    }

    public static void main(String[] args){
        String a = "middle";
        System.out.println(middle(a));
    }
}