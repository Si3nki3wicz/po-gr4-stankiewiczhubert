package pl.edu.uwm.wmii.StankiewiczHubert.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad1male2lab04 {
    public static int coutChar (File nazwa, char c) throws FileNotFoundException{
        int licznik = 0;
        Scanner scan = new Scanner(nazwa);
        String str = scan.nextLine();
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c){
                licznik++;
            }
        }
        return licznik;
    }


    public static void main(String[] args) throws FileNotFoundException{

        char a = 'c';
        System.out.print("Literka c powtorzyla sie " + coutChar(new File("plik.txt"),a) + " razy");
    }
}
