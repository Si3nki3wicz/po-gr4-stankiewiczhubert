package pl.edu.uwm.wmii.StankiewiczHubert.java12;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class zad8java12 {
    public static <E> void print(Iterable<E> list){
        Iterator<E> iterator = list.iterator();
        while(iterator.hasNext()) {
            E element = iterator.next();
            System.out.print( element);
            if(iterator.hasNext()){
                System.out.print(", ");
            }
        }
    }
    public static void main(String[] args) {

        List<String> list = new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");

        print(list);

}
}
