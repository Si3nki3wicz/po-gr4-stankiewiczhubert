package pl.edu.uwm.wmii.StankiewiczHubert.java12;

import java.util.*;

public class zad5java12 {


        public static void main(String[] args) {
            String str = "Ala ma kota. Jej kot lubi myszy.";
            System.out.println(str);
            odwroc1(str);

        }
    public static void odwroc1(String str) {
        Stack<String> stack = new Stack<>();

        String[] slowa = str.split(" ");

        int slowoWKolei, liczbaSlow;

        for (String slowo : slowa) {
            if (slowo.charAt(slowo.length() - 1) != '.') {
                stack.push(slowo);
            } else {
                stack.push(slowo);

                slowoWKolei = 1;
                liczbaSlow = stack.size();

                while (!stack.empty()) {
                    String slowoDoDruku = stack.pop();

                    if (slowoWKolei == 1) {

                        slowoDoDruku = slowoDoDruku.substring(0, slowoDoDruku.length() - 1).toLowerCase();

                        slowoDoDruku = slowoDoDruku.substring(0, 1).toUpperCase() + slowoDoDruku.substring(1);

                    } else if (slowoWKolei == liczbaSlow) {
                        slowoDoDruku = slowoDoDruku.toLowerCase() + ".";
                    }

                    System.out.print(slowoDoDruku + " ");

                    slowoWKolei++;
                }
            }
        }
    }
}


