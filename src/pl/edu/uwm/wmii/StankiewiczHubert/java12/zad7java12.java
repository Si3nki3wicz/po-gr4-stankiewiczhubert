package pl.edu.uwm.wmii.StankiewiczHubert.java12;

import java.util.Scanner;

public class zad7java12 {

    public static void eratos(int zakres){
        int x, k;
        int tablica[] = new int[zakres+1];
        int dok = (int) Math.floor(Math.sqrt(zakres));

        for (int i=1; i<=zakres; i++){
            tablica[i]=i;
        }

        for (int i=2; i<=dok; i++) {
            if (tablica[i] != 0) {
                x = i+i;
                while (x<=zakres) {
                    tablica[x] = 0;
                    x += i;
                }
            }
        }
        for (k=2; k<=zakres; k++) if (tablica[k]!=0){
            System.out.print(k + ", ");
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj gorny zakres, do ktorego chcesz odnalezc liczby pierwsze\n");
        Scanner xin = new Scanner(System.in);
        System.out.println("Podaj wartosc:");
        int zakres = xin.nextInt();

        System.out.println("Liczby pierwsze z zakresu od 1 do " + zakres);
        eratos(zakres);
    }
}
