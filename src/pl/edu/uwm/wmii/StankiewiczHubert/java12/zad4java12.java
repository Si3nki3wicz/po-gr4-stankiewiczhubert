package pl.edu.uwm.wmii.StankiewiczHubert.java12;

import java.util.LinkedList;

public class zad4java12 {
    public static void main(String[] args) {
        LinkedList pracownicy = new LinkedList<Integer>();
        pracownicy.add(1);
        pracownicy.add(2);
        pracownicy.add(3);
        pracownicy.add(4);


        System.out.println("Lista przed zmianami");
        wypisz(pracownicy);
        odwroc(pracownicy);
        System.out.println("\n" + "Lista po zmianach");
        wypisz(pracownicy);

    }
    public static <T extends Comparable<T>> void wypisz (LinkedList<T> lista){
        for(int i = 0; i < lista.size(); i++){
            System.out.print(lista.get(i) + ", ");
        }
    }
    public static <T extends Comparable<T>> void odwroc (LinkedList<T> lista){
        for (int i = 0; i < lista.size()/2; i++) {
            T temp = lista.get(i);
            lista.set(i, lista.get(lista.size() - 1 - i));
            lista.set(lista.size() - 1 - i, (T) temp);
        }
    }
}