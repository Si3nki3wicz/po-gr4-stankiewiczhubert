package pl.edu.uwm.wmii.StankiewiczHubert.java12;

import java.util.LinkedList;

public class zad3java12 {
    public static void main(String[] args) {
        LinkedList pracownicy = new LinkedList<String>();
        pracownicy.add("0");
        pracownicy.add("1");
        pracownicy.add("2");
        pracownicy.add("3");


        System.out.println("Lista przed zmianami");
        wypisz(pracownicy);
        odwroc(pracownicy);
        System.out.println("\n" + "Lista po zmianach");
        wypisz(pracownicy);

    }
    public static <T extends Comparable<T>> void wypisz (LinkedList<T> lista){
        for(int i = 0; i < lista.size(); i++){
            System.out.print(lista.get(i) + ", ");
        }
    }
    public static void odwroc (LinkedList<String> lista){
        for (int i = 0; i < lista.size()/2; i++) {
            String temp = lista.get(i);
            lista.set(i, lista.get(lista.size() - 1 - i));
            lista.set(lista.size() - 1 - i, temp);
        }
    }
}