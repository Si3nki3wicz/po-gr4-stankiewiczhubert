package pl.imiajd.Stankiewicz;

import java.util.Objects;

interface IcomClone extends Comparable,Cloneable{
}

public class Autor implements IcomClone {

    public Autor(String nazwa, String email, char plec) {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;
    }

    public String getNazwa(){
        return nazwa;
    }
    public String getEmail(){
        return email;
    }
    public char getPlec(){
        return plec;
    }

    void setNazwa(String a){
        nazwa = a;
    }
    void setEmail(String b){
        email = b;
    }
    void setPlec(char c){
        plec = c;
    }

    private String nazwa;
    private String email;
    private char plec;


    @Override
    public int compareTo(Object o) {
        if (nazwa.charAt(0) > ((Autor)o).nazwa.charAt(0)) {
            return 1;
        } else if (nazwa.charAt(0) < ((Autor)o).nazwa.charAt(0)) {
            return -1;
        } else {
            if (email.charAt(0) > ((Autor)o).email.charAt(0)) {
                return 1;
            } else if (email.charAt(0) < ((Autor)o).email.charAt(0)) {
                return -1;
            }
                if(plec > ((Autor)o).plec){
                    return 1;
                }else if(plec < ((Autor)o).plec){
                    return -1;
                }
                else{
                    return 0;
                }
        }

    }
    @Override
    public String toString() {
        return "Autor" +"[" + nazwa + " " + "e-mail" + "email='" + email + " " + "plec" + "plec='" + plec + "]" + "\n";
    }
}