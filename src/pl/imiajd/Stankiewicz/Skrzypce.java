package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Skrzypce extends Instrument{

    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    public String dzwięk() {
        return "Dźwięk skrzypiec";
    }

    public String toString(){
        return "Skrzypce - " +" producent: "+ getProducent() +", dataprodukcji: "+getRokProdukcji();
    }
}