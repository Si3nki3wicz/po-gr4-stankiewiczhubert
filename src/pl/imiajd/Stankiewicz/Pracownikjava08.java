package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Pracownikjava08 extends Osobajava08 {

    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownikjava08(String nazwisko, String[] imiona, LocalDate dataUroddzenia, boolean płeć, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUroddzenia, płeć);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory) + " data zatrudnienia: " + getDataZatrudnienia();
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }
}
