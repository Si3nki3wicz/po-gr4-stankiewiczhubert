package pl.imiajd.Stankiewicz;

public class Osoba {

    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getNazwisko() { //z generatora
        return nazwisko;
    }

    public int getRokUrodzenia() { // z generatora
        return rokUrodzenia;
    }

    public String toString() {
        return "Osoba = nazwisko: " + nazwisko + ", rok urodzenia: " + rokUrodzenia;
    }
}
