package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Fortepian extends Instrument {

    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    public String dzwięk() {
        return "Dźwięk fortepianu";
    }

    public String toString(){
        return "Fortepian - " +" producent: "+ getProducent() +", dataprodukcji: "+getRokProdukcji();
    }
}
