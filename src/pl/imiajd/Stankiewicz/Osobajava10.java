package pl.imiajd.Stankiewicz;

import java.time.LocalDate;
import java.util.Objects;

public class Osobajava10 implements  Cloneable, Comparable<Osobajava10>{

    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osobajava10(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }



    @Override
    public String toString() {
        return "Osobajava10[" +
                "nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + dataUrodzenia +
                ']';
    }

    @Override
    public boolean equals(Object x) {
        if (this == x) return true;
        if (x == null || getClass() != x.getClass()) return false;
        Osobajava10 that = (Osobajava10) x;
        return Objects.equals(nazwisko, that.nazwisko) &&
                Objects.equals(dataUrodzenia, that.dataUrodzenia);
    }


    @Override
    public int compareTo(Osobajava10 o) {
        return this.nazwisko.compareTo(o.nazwisko);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }
}
