package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Flet  extends Instrument{

    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }


    public String dzwięk() {
        return "Dźwięk fleta";
    }

    public String toString(){
        return "Flet - " +" producent: "+ getProducent() +", dataprodukcji: "+getRokProdukcji();
    }
}
