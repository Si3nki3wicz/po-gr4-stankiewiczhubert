package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Studentjava10 extends Osobajava10 implements Cloneable, Comparable<Osobajava10>{

    private double sredniaOcen;

    public Studentjava10(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }



    @Override
    public String toString() {
        return "Osobajava10{" +
                "nazwisko='" + getNazwisko() + '\'' +
                ", dataUrodzenia=" + getDataUrodzenia() + ", średnia ocen=" +sredniaOcen+
                '}';
    }
}