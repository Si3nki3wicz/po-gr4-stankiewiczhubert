package pl.imiajd.Stankiewicz;

public class Nauczyciel extends Osoba {

    private double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja() { // z generatora
        return pensja;
    }

    public String toString() {
        return "Nauczyciel = nazwisko: " + getNazwisko() + ", rok urodzenia: " + getRokUrodzenia() + ", pensja: " + pensja;
    }
}
