package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public abstract class Osobajava08 {

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUroddzenia;
    private boolean płeć;

    public Osobajava08(String nazwisko, String[] imiona, LocalDate dataUroddzenia, boolean płeć) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUroddzenia = dataUroddzenia;
        this.płeć = płeć;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String getImiona() {
        StringBuffer str = new StringBuffer();
        for(int i=0;i< imiona.length;i++){
            str.append(imiona[i]);
            str.append(" ");
        }
        return str.toString();
    }

    public LocalDate getDataUroddzenia() {
        return dataUroddzenia;
    }

    public boolean getPłeć() {
        return płeć;
    }
}