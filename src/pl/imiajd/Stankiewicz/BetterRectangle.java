package pl.imiajd.Stankiewicz;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int w, int h){

        //setLocation(0,0);
        //setSize(w,h);
        super(w,h);
    }

    public double GetArea(){
        return getWidth() * getHeight();
    }
    public double GetPerimeter(){
        return (getWidth() + getHeight())*2;
    }




}
