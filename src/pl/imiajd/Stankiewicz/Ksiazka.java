package pl.imiajd.Stankiewicz;


public class Ksiazka implements IcomClone {
    public Ksiazka(String tytul, String autor, double cena)
    {
        this.tytul = tytul;
        this.autor =autor;
        this.cena = cena;
    }
    private String tytul;
    private String autor;
    private double cena;

    @Override
    public int compareTo(Object o) {
        if (tytul.charAt(0) > ((Ksiazka)o).tytul.charAt(0)) {
            return 1;
        } else if (autor.charAt(0) < ((Ksiazka)o).tytul.charAt(0)) {
            return -1;
        } else {
            if (autor.charAt(0) > ((Ksiazka)o).autor.charAt(0)) {
                return 1;
            } else if (autor.charAt(0) < ((Ksiazka)o).autor.charAt(0)) {
                return -1;
            }
            if(cena > ((Ksiazka)o).cena){
                return 1;
            }else if(cena < ((Ksiazka)o).cena){
                return -1;
            }
            else{
                return 0;
            }
        }

    }

}
