package pl.imiajd.Stankiewicz;

import java.time.LocalDate;

public class Studentjava08 extends Osobajava08 {

    private String kierunek;
    private double średniaOcen;

    public Studentjava08(String nazwisko, String[] imiona, LocalDate dataUroddzenia, boolean płeć, String kierunek) {
        super(nazwisko, imiona, dataUroddzenia, płeć);
        this.kierunek = kierunek;

    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + " średnia ocen: " +getŚredniaOcen();
    }

    public void setŚredniaOcen(double średniaOcen) {
        this.średniaOcen = średniaOcen;

    }

    public double getŚredniaOcen() {
        return średniaOcen;
    }
}
